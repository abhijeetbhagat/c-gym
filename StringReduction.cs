﻿using System;
using System.Linq;

namespace StringReduction
{
    class Program
    {
        static void Main(string[] args)
        {
            Int32 map = 0;
            var strng = "pprrqq";
            
            var output = String.Join("", strng.Where((c) =>
            {
                var flag = false;
                var shft = 1 << (c % 97);
                if ((map & shft) == 0) //check
                {
                    flag = true;
                }
                map = map | shft; //remember
                return flag;
            }
            ));
            //output - prq
        }
    }
}
