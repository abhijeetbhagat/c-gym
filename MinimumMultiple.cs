﻿using System.Linq;
using System.Threading.Tasks;

class Program
{

    static int gcd(int a, int b)
    {
        if (a > b)
            return gcd(a - b, b);
        else if (b > a)
            return gcd(a, b - a);
        else
            return a;
    }

    static void Main(string[] args)
    {
        var l = new int[] { 21, 6 };
        var prdt = Task.Run(() => l.Aggregate(1, (x, y) => x * y)); //product
        var gcdt = Task.Run(() => l.Aggregate(l.First(), (x, y) => gcd(x, y))); //gcd
        prdt.Wait();
        gcdt.Wait();
        var outp = prdt.Result / gcdt.Result; //lcm
    }
}
